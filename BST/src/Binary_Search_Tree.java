/*Binary search tree
 * this program implements the BST operations
 * insert
 * delete 
 * find
 * display
 * */
//main class for BST
public class Binary_Search_Tree
{
	public static  Node root;
	public Binary_Search_Tree()
	{
		this.root = null;
	}
	
    //insert method for insert and element into tree
	//@element - user input which they want insert into tree
	public void insert(int element)
	{
		System.out.println("insert into insert method");
		Node newNode = new Node(element);
		if(root==null)
		{
			root = newNode;
			return;
		}
		Node current = root;
		Node parent = null;
		while(true){//loop until new node get it's position
			parent = current;
			if(element<current.data){				
				current = current.left;
				if(current==null){
					parent.left = newNode;
					return;
				}
			}else{
				current = current.right;
				if(current==null){
					parent.right = newNode;
					System.out.println("exit from insert method");
					return;
				}
			}
		}
		
	}
	//delete method to delete an element from queue
	public boolean delete(int element){
		System.out.println("insert into delete method");
		Node parent = root;
		Node current = root;
		boolean isLeftChild = false;
		while(current.data!=element){
			parent = current;
			if(current.data>element){
				isLeftChild = true;
				current = current.left;
			}else{
				isLeftChild = false;
				current = current.right;
			}
			if(current ==null){
				return false;
			}
			
		}
		
		//Case 1: if node to be deleted has no children
		if(current.left==null && current.right==null){
			if(current==root){
				root = null;
			}
			if(isLeftChild ==true){
				parent.left = null;
			}else{
				parent.right = null;
			}
		}
		//Case 2 : if node to be deleted has only one child
		else if(current.right==null){
			if(current==root){
				root = current.left;
			}else if(isLeftChild){
				parent.left = current.left;
			}else{
				parent.right = current.left;
			}
		}
		else if(current.left==null){
			if(current==root){
				root = current.right;
			}else if(isLeftChild){
				parent.left = current.right;
			}else{
				parent.right = current.right;
			}
		}else if(current.left!=null && current.right!=null){
			
			//now we have found the minimum element in the right sub tree
			Node successor	 = getSuccessor(current);
			if(current==root){
				root = successor;
			}else if(isLeftChild){
				parent.left = successor;
			}else{
				parent.right = successor;
			}			
			successor.left = current.left;
		}		
		 System.out.println("exit from delete method");
		return true;		
	}
	
	//method to find next highest node than the node which we want to delete
	public Node getSuccessor(Node deleleNode){
		Node successsor =null;
		Node successsorParent =null;
		Node current = deleleNode.right;
		while(current!=null){
			successsorParent = successsor;
			successsor = current;
			current = current.left;
		}
		//check if successor has the right child, it cannot have left child for sure
		// if it does have the right child, add it to the left of successorParent.
//		successsorParent
		if(successsor!=deleleNode.right){
			successsorParent.left = successsor.right;
			successsor.right = deleleNode.right;
		}
		return successsor;
	}
	public boolean find(int element){
		System.out.println("insert into find method");
		Node current = root;
		while(current!=null){
			if(current.data==element){
				return true;
			}else if(current.data>element){
				current = current.left;
			}else{
				current = current.right;
			}
		}
		System.out.println("exit from find method");
		return false;
	}
	//display method for display tree elements
	public void display(Node root)
	{
		System.out.println("insert into display method");
		if(root!=null){
			display(root.left);
			System.out.print(" " + root.data);
			display(root.right);
		}
		System.out.println("insert into display method");
	}
	//main method
	public static void main(String arg[]){
		Binary_Search_Tree b = new Binary_Search_Tree();//create object of class Binary_Search_Tree
		b.insert(3);b.insert(8);
		b.insert(1);b.insert(4);b.insert(6);b.insert(2);b.insert(10);b.insert(9);
		b.insert(20);b.insert(25);b.insert(15);b.insert(16);
		System.out.println("Original Tree : ");
		b.display(b.root);		
		System.out.println("");
		System.out.println("Check whether Node with value 4 exists : " + b.find(4));
		System.out.println("Delete Node with no children (2) : " + b.delete(2));		
		b.display(root);
		System.out.println("\n Delete Node with one child (4) : " + b.delete(4));		
		b.display(root);
		System.out.println("\n Delete Node with Two children (10) : " + b.delete(10));		
		b.display(root);
	}
}

//class that create new node
class Node{
	int data;
	Node left;
	Node right;	
	public Node(int data){
		this.data = data;
		left = null;
		right = null;
	}
}
