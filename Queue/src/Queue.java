import java.util.Scanner;
/*
 * Queue
 * this code emplement queue operation
 * insert
 * delete
 * display
 * using Array
 * 
 * @author Sweta Dhandhukiya 
 * 
 * */
public class Queue// class for queue methods
{
	int rear = -1;
	int front = -1;
	int q[] = new int[5];
	int size = q.length;
	
	//insert method to insert an element into queue
	void Insert(int value)//value is the parameter which user wants to insert into queue
	{
		
		System.out.println("eneter into insert method");
		System.out.println("");
		if(rear==size-1 && front == 0 )
		{
			System.out.println("Queue is full");
			rear = 0;
		}
		else
		{
			rear = rear+1;
			q[rear] = value;
			
		}
		if(front==-1)
		{
			front = 0;
		}
		
		System.out.println("exit from insert method");

	}
	//delete method to delete an element from queue
	void Delete()
	{ 	
		System.out.println("insert into delete method");
		int y;
		 if(front==-1)
		    {
			 	System.out.println("Circular Queue is underflow");
		         return;
		     }
		     y=q[front];
		     q[front]=0;
		     front=front+1;
		     System.out.println("front after deletion---------"+front);
		     System.out.println("exit from delete method");
	}
	//display method for display queue elements
	void Display()
	{
		System.out.println("insert into display method");//loop until queue is empty
		
		    		for ( int i = 0; i < size; i ++ )
			        {
			            System.out.println (q[ i ]);
			        }
		    	
		 System.out.println("exit from display method");
	}
	
	//main method
	public static void main(String a[])
	{
				
		Scanner sc = new Scanner(System.in);
		Queue queue = new Queue();//object of queue
		int choice, value;

	    do
	    {
	    	System.out.println("enter your choice");
			System.out.println("1.Insert");
			System.out.println("2.Delete");
			System.out.println("3.display");
	        System.out.println("4: Exit");

	        System.out.print ("\nChoice: ");
	        choice = sc.nextInt();

	        switch ( choice )
	        {
	            case 1:
	            	System.out.println("enter your value");
	        		value = sc.nextInt();//takes value from user to insert into queue
	        		queue.Insert(value);
	                break;
	            case 2:
	            	queue.Delete();
	                break;
	            case 3:
	            	queue.Display();
	                break;
	            case 4:
	                System.out.println ("Exit");
	                break;
	            default:
	                System.out.println ("Wrong choice!");
	                break;
	        }

	    } while ( choice != 4 );

		
	}
		
		
}