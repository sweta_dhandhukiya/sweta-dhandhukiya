package queue_stack_test;
/*
 * Queue using stack 
 * this program implement the queue operation 
 * insert
 * delete
 * display
 * using stack
 * 
 * @author Sweta Dhandhukiya
 * */
import java.util.HashMap;
import java.util.Scanner;
import java.util.Stack;
//main class
public class QueueUsingStack 
{
	int i;
	Stack stack = new Stack();
	Stack tmp = new Stack();
	HashMap hm = new HashMap();
	Scanner s = new Scanner(System.in);
	
	//insert method for insert and element into queue
	//@element - user input which they want insert into queue
	//@size - queue size enter by user
	void Insert(int element,int size)
	{
		System.out.println("enter into insert method");
			stack.push(element);
			
		System.out.println("exit from insert method");

		
	}
	
	//delete  method which delete the element which come first
	void Delete()
	{
		System.out.println("enter into delete method");
		//for(i=0; i<stack.size();i++)
		while(!stack.isEmpty())//loop until original stack is not empty
		{
			tmp.push(stack.pop());
		}
		tmp.pop();
		
		//for(i=0; i<tmp.size();i++)
		while(!tmp.isEmpty())////loop until temporory stack is not empty
		{
			stack.push(tmp.pop());
		}
		System.out.println("enter into insert method");
		
	}
	
	//display method for display queue element
	void Display()
	{
		System.out.println("enter into delete method");
		for(i=0; i<stack.size();i++)
		{
			System.out.println("queue element:"+stack.get(i));
		}
		System.out.println("enter into insert method");
	}

	//main method
	public static void main(String a[])
	{
		Scanner sc = new Scanner(System.in);
		
		//create object of queue
		QueueUsingStack queue = new QueueUsingStack();
		int choice, value,size;
		
		System.out.println("enter queue size");
		size = sc.nextInt();//takes stack size from user
		
		 do
		    {
		    	System.out.println("enter your choice");
				System.out.println("1.Insert");
				System.out.println("2.Delete");
				System.out.println("3.display");
		        System.out.println("4: Exit");

		        System.out.print ("\nChoice: ");
		        choice = sc.nextInt();

		        switch ( choice )
		        {
		            case 1:
		            	System.out.println("enter your value");
		        		value = sc.nextInt();//takes value from user to insert into queue
		        		queue.Insert(value,size);
		                break;
		            case 2:
		            	queue.Delete();
		                break;
		            case 3:
		            	queue.Display();
		                break;
		            case 4:
		                System.out.println ("Exit");
		                break;
		            default:
		                System.out.println ("Wrong choice!");
		                break;
		        }

		    } while ( choice != 4 );

			
		}
}
